package br.fpf.forge;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.HashMap;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.jboss.forge.parser.JavaParser;
import org.jboss.forge.parser.java.JavaClass;
import org.jboss.forge.parser.java.JavaInterface;
import org.jboss.forge.project.Project;
import org.jboss.forge.project.dependencies.Dependency;
import org.jboss.forge.project.dependencies.DependencyBuilder;
import org.jboss.forge.project.facets.DependencyFacet;
import org.jboss.forge.project.facets.JavaSourceFacet;
import org.jboss.forge.project.facets.WebResourceFacet;
import org.jboss.forge.resources.FileResource;
import org.jboss.forge.resources.java.JavaResource;
import org.jboss.forge.shell.ShellPrompt;
import org.jboss.forge.shell.plugins.Alias;
import org.jboss.forge.shell.plugins.Command;
import org.jboss.forge.shell.plugins.Option;
import org.jboss.forge.shell.plugins.PipeOut;
import org.jboss.forge.shell.plugins.Plugin;
import org.jboss.forge.shell.plugins.RequiresProject;
import org.jboss.seam.render.TemplateCompiler;
import org.jboss.seam.render.template.CompiledTemplateResource;

@Alias("speeder")
@RequiresProject
public class SpeederPlugin implements Plugin {

	@Inject
	private Project project;

	@Inject
	private ShellPrompt prompt;

	private final Dependency springData = DependencyBuilder
			.create("org.springframework.data:spring-data-jpa:[1.1.0.BUILD-SNAPSHOT]");


	@Inject
	private TemplateCompiler compiler;

	@Command("scallfold")
	public void run(
			PipeOut out,
			@Option(name = "entities", required = true) final JavaResource entitySource)
			throws FileNotFoundException {
		out.println("Executed command with value: " + entitySource);
		setupDependencies();
		
		JavaSourceFacet javaFacet = project.getFacet(JavaSourceFacet.class);
		
		// viewBean.setPackage(java.getBasePackage() + ".view");
		// viewBean.addAnnotation(SEAM_PERSIST_TRANSACTIONAL_ANNO);
		//createClass(project, "/PersistenceUtils.jv", true);
		
		
		renderInterface("/ResourceInterfaceTemplate.jv",entitySource, "rest");
		renderClass("/ResourceImplTemplate.jv",entitySource, "rest.resources");
		System.out.println();
		renderInterface("/ServiceInterfaceTemplate.jv",entitySource, "application");
		renderClass("/ServiceImplTemplate.jv",entitySource, "application.beans");
		
		renderInterface("/RepositoryInterfaceTemplate.jv",entitySource, "repository");
		//renderClass("/RepositorySpringDataJPA.jv",entitySource, "repository");
		
	}
	
	public void renderClass(String template, JavaResource entitySource,String package_) throws FileNotFoundException{
		JavaClass entity = (JavaClass) entitySource.getJavaSource();

		JavaSourceFacet javaFacet = project.getFacet(JavaSourceFacet.class);

		CompiledTemplateResource serviceTemplate = compiler //TODO fazer cache
				.compile(template);
		HashMap<Object, Object> context = new HashMap<Object, Object>();
		context.put("entity", entity);
		context.put("basePackage",javaFacet.getBasePackage());

		// Create the Backing Bean for this entity
		JavaClass service = JavaParser.parse(JavaClass.class,
				serviceTemplate.render(context));
		service.setPackage(javaFacet.getBasePackage() + "."+package_);
		SpeederPlugin.createOrOverwrite(prompt,
				javaFacet.getJavaResource(service), service.toString(), true);
	}
	
	public void renderInterface(String template, JavaResource entitySource,String package_) throws FileNotFoundException{
		JavaClass entity = (JavaClass) entitySource.getJavaSource();

		JavaSourceFacet javaFacet = project.getFacet(JavaSourceFacet.class);
		//atualizar xml, webFacet.

		CompiledTemplateResource serviceTemplate = compiler //TODO fazer cache
				.compile(template);
		HashMap<Object, Object> context = new HashMap<Object, Object>();
		context.put("entity", entity);

		// Create the Backing Bean for this entity
		JavaInterface service = JavaParser.parse(JavaInterface.class,
				serviceTemplate.render(context));

		service.setPackage(javaFacet.getBasePackage() + "."+package_);
		SpeederPlugin.createOrOverwrite(prompt,
				javaFacet.getJavaResource(service), service.toString(), true);
	}

	public void createClass(final Project project, String javaTemplate,
			final boolean overwrite) {
		ClassLoader loader = Thread.currentThread().getContextClassLoader();
		JavaClass util = JavaParser.parse(JavaClass.class,
				loader.getResourceAsStream(javaTemplate));

		JavaSourceFacet java = project.getFacet(JavaSourceFacet.class);

		try {
			JavaResource utilResource = java.getJavaResource(util);

			SpeederPlugin.createOrOverwrite(prompt, utilResource,
					util.toString(), true);
		} catch (FileNotFoundException e) {
			throw new RuntimeException(e);
		}
	}

	public static void createOrOverwrite(final ShellPrompt prompt,
			final FileResource<?> resource, final InputStream contents,
			final boolean overwrite) {
		if (!resource.exists()
				|| overwrite
				|| prompt.promptBoolean("[" + resource.getFullyQualifiedName()
						+ "] File exists, overwrite?")) {
			resource.createNewFile();
			resource.setContents(contents);
		}
	}

	public static void createOrOverwrite(final ShellPrompt prompt,
			final FileResource<?> resource, final String contents,
			final boolean overwrite) {
		if (!resource.exists()
				|| overwrite
				|| prompt.promptBoolean("[" + resource.getFullyQualifiedName()
						+ "] File exists, overwrite?")) {
			resource.createNewFile();
			resource.setContents(contents);
		}
	}

	public void setupDependencies() {
		DependencyFacet df = project.getFacet(DependencyFacet.class);
		if (!df.hasDirectDependency(springData)) {
			df.addRepository("spring-milestone", "http://oss.sonatype.org/content/repositories/springsource-all");
			df.addDirectDependency(prompt.promptChoiceTyped(
					"Install which version of Spring Data?",
					df.resolveAvailableVersions(springData)));
		}

	}
	
	/*@Command("repository")
	public void repository(
			PipeOut out,
			@Option(name = "entity", required = true) final JavaResource entitySource)
			throws FileNotFoundException {
		JavaSourceFacet javaFacet = project.getFacet(JavaSourceFacet.class);
		String basePackage = javaFacet.getBasePackage();
		
	}*/
}

/*
 * TODO Setup libraries in the pom
 */
